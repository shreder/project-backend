const path = require('path');
const express = require('express');
const exphbs = require('express-handlebars');

// Constants
const HOST = '0.0.0.0';
const PORT = 8000;

// App
const app = express();
app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    extname: '.hbs',
    layoutsDir: path.join(__dirname, 'views/layouts')
}));

// Templates
app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, 'views'));

// Handlers
app.get('/', (request, response) => {
    response.render('home', {
        name: 'John'
    })
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);